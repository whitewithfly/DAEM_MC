# Cmc Iso
`Cmc Iso` is a Monte-Carlo estimator of the reaction rate based on the DAEM (Distributed 
Activation Energy Model) in a uniform temperature media. The model assumed a gaussian 
distribution of the activation energy. Additionally one can compare the obtain 
estimation with a determinist calculus based on a gauss quadrature.  


## How to build
`Cmc Iso` relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build. It also depends
on the [GSL](https://www.gnu.org/software/gsl/),
[RSys](https://gitlab.com/vaplv/rsys/),
[Star-SP](https://gitlab.com/meso-star/stat-sp/) and
[Star-MC](https://gitlab.com/meso-star/stat-mc/) libraries. It is compatible
GNU/Linux on x86-64 architectures.

First make sure that CMake is installed on your system. Then install the RCMake
package as well as the aforementioned prerequisites. Finally, generate the
project from the `cmake / CMakeLists.txt` file by adding to the`
CMAKE_PREFIX_PATH` variable the installation directories for these
dependencies. The resulting project can be edited, built, tested, and installed
like any CMake project. Refer to [CMake] (https://cmake.org/documentation) for
more information on CMake.

To build and install, from the base directory do
~~~shell
mkdir build && cd build
cmake ../cmake -DCMAKE_PREFIX_PATH=/path/to/star-engine/local/
make
~~~


## Usage 
~~~shell
./cmc_iso [-t time]  [-d deterministic]   [-h help]   [-n realisation(s)]	[-T initial temperature]
  [-o order] [-A pre-exponential factor] [-e gaussian mean energy] [-w gaussian width]
~~~


## Copyright notice
Copyright (C) 2021 Université Paul Sabatier (<yaniss.nyffenegger-pere@laplace.univ-tlse.fr>).  


## License
`Cmc Iso` is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
