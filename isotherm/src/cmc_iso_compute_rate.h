/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef CMC_ISO_REAL_FUNC_H
#define CMC_ISO_REAL_FUNC_H

#include <rsys/rsys.h>

/* forward definition */
struct ssp_rng;

/*******************************************************************************
 * MC realization function
 ******************************************************************************/
extern LOCAL_SYM res_T
daem_realization
	(void* length, 
	 struct ssp_rng* rng, 
	 const unsigned ithread, 
	 void* context);

#endif /* CMC_ISO_PARA_FUNC_H */
