/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "cmc_iso.h"
#include "cmc_iso_log.h"
#include <rsys/logger.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_out(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)ctx;
  fprintf(stderr, CMC_ISO_LOG_INFO_PREFIX"%s", msg);
}

static void
print_err(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)ctx;
  fprintf(stderr, CMC_ISO_LOG_ERROR_PREFIX"%s", msg);
}

static void
print_warn(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)ctx;
  fprintf(stderr, CMC_ISO_LOG_WARNING_PREFIX"%s", msg);
}

static void
log_msg
  (struct cmc_iso* cmc_iso,
   const enum log_type stream,
   const char* msg,
   va_list vargs)
{
  ASSERT(cmc_iso && msg);
  CHK(logger_vprint(&cmc_iso->logger, stream, msg, vargs) == RES_OK);
}

/*******************************************************************************
 * API
 ******************************************************************************/
void
cmc_iso_log(struct cmc_iso* cmc_iso, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(cmc_iso && msg);
  va_start(vargs_list, msg);
  log_msg(cmc_iso, LOG_OUTPUT, msg, vargs_list);
  va_end(vargs_list);
}

void
cmc_iso_log_err(struct cmc_iso* cmc_iso, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(cmc_iso && msg);
  va_start(vargs_list, msg);
  log_msg(cmc_iso, LOG_ERROR, msg, vargs_list);
  va_end(vargs_list);
}

void
cmc_iso_log_warn(struct cmc_iso* cmc_iso, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(cmc_iso && msg);
  va_start(vargs_list, msg);
  log_msg(cmc_iso, LOG_WARNING, msg, vargs_list);
  va_end(vargs_list);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
setup_logger(struct cmc_iso* cmc_iso)
{
  logger_init(NULL, &cmc_iso->logger);
  logger_set_stream(&cmc_iso->logger, LOG_OUTPUT, print_out, NULL);
  logger_set_stream(&cmc_iso->logger, LOG_ERROR, print_err, NULL);
  logger_set_stream(&cmc_iso->logger, LOG_WARNING, print_warn, NULL);
}

