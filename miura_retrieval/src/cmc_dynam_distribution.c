/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "cmc_dynam.h"
#include "cmc_dynam_log.h"
#include "cmc_dynam_distribution.h"

#include <rsys/clock_time.h>
#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>

struct cmc_dynam_distrib {
  int count;
  double* energy_value;
  double* density_value;

  ref_T ref;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T 
load_distribution_props
  (struct cmc_dynam* cmc_dynam,
	 struct cmc_dynam_distrib* distrib,
	 const char* distrib_props_filename)
{
  FILE *file_species = NULL;
	res_T res = RES_OK;
  int i;
  ASSERT(distrib_props_filename);

  file_species = fopen(distrib_props_filename, "r");
  if(!file_species) {
		cmc_dynam_log_err(cmc_dynam, "%s: Cannot open `%s'\n", FUNC_NAME, distrib_props_filename);
    res = RES_IO_ERR;
    goto error;
  }

  CHK(fscanf(file_species, "%d", &distrib->count) == 1);
	CHK(distrib->energy_value = mem_alloc((size_t)distrib->count* sizeof(double)));
  CHK(distrib->density_value = mem_alloc((size_t)distrib->count*sizeof(double)));

  FOR_EACH(i, 0, distrib->count) {
    CHK(fscanf(file_species, "%lf %lf ", 
    &distrib->energy_value[i],
    &distrib->density_value[i]) == 2); 
		/*distrib->energy_value[i] *= 1e3; KJoule to Joule conversion*/
  }

  if(i < distrib->count) {
		cmc_dynam_log_err(cmc_dynam, "%s: Not enough data.\n", FUNC_NAME);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  if(file_species) fclose(file_species);
  return res;
error:
  goto exit;
}


static void
release_distrib(ref_T* ref)
{
  struct cmc_dynam_distrib* distrib = NULL;
  ASSERT(ref);

  distrib = CONTAINER_OF(ref, struct cmc_dynam_distrib, ref);
	if(distrib->energy_value) mem_rm(distrib->energy_value);
	if(distrib->density_value) mem_rm(distrib->density_value);
 	mem_rm(distrib); 
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
cmc_dynam_distrib_create
  (struct cmc_dynam* cmc_dynam,
   const char* distrib_props_filename,
   struct cmc_dynam_distrib** out_distrib)
{
  char buf[512];
  struct time t0, t1, elapsed_time;
  struct cmc_dynam_distrib* distrib = NULL;
	res_T res = RES_OK;
  ASSERT(distrib_props_filename && out_distrib);

  CHK(distrib = mem_calloc(1, sizeof(struct cmc_dynam_distrib)));
	ref_init(&distrib->ref);

	time_current(&t0);
  res = load_distribution_props(cmc_dynam, distrib, distrib_props_filename);
  if(res != RES_OK) goto error;
  time_current(&t1);
	time_sub(&elapsed_time, &t1, &t0);

  time_dump(&elapsed_time, TIME_ALL, NULL, buf, sizeof(buf));
  cmc_dynam_log(cmc_dynam, "Load distribution data in %s\n", buf);

exit:
  *out_distrib = distrib;
  return res;
error:
  if(distrib) {
    cmc_dynam_distrib_ref_put(distrib);
    distrib = NULL;
  }
  goto exit;
}

res_T
compute_cumulative_distribution
  (struct cmc_dynam* cmc_dynam)
{
	int i, number_values;
	res_T res = RES_OK;
	ASSERT(cmc_dynam);
	
	number_values = cmc_dynam_distrib_get_count(cmc_dynam->distrib);
	CHK(cmc_dynam->cumulative = mem_alloc((size_t)(number_values)*sizeof(double)));
	

	/*cmc_dynam->cumulative[0] = 0;
	FOR_EACH(i, 0, number_values) {	
		cmc_dynam->cumulative[i+1] = 
		cmc_dynam->cumulative[i] + cmc_dynam_distrib_get_density(cmc_dynam->distrib)[i]; 
	}
	FOR_EACH(i, 0, number_values+1) {	
		cmc_dynam->cumulative[i] /= cmc_dynam->cumulative[number_values]; 
	}*/
			
	cmc_dynam->cumulative[0] = 0;
	FOR_EACH(i, 0, number_values-1) {	
		cmc_dynam->cumulative[i+1] = 
		cmc_dynam->cumulative[i] +
		(cmc_dynam_distrib_get_density(cmc_dynam->distrib)[i+1]
		+ cmc_dynam_distrib_get_density(cmc_dynam->distrib)[i] )*0.5
		* (cmc_dynam_distrib_get_energy(cmc_dynam->distrib)[i+1] 
		- cmc_dynam_distrib_get_energy(cmc_dynam->distrib)[i]);
	}
	FOR_EACH(i, 0, number_values) {	
		cmc_dynam->cumulative[i] /= cmc_dynam->cumulative[number_values-1]; 
	}

	if(cmc_dynam->cumulative[number_values-1] < 0.9999) {
 		res = RES_BAD_ARG; 
		goto error;
	}

exit:
  return res;
error:
  goto exit;
}

void
cmc_dynam_distrib_ref_get(struct cmc_dynam_distrib* distrib)
{
  ASSERT(distrib);
  ref_get(&distrib->ref);
}

void
cmc_dynam_distrib_ref_put(struct cmc_dynam_distrib* distrib)
{
  ASSERT(distrib);
  ref_put(&distrib->ref, release_distrib);
}

int 
cmc_dynam_distrib_get_count(const struct cmc_dynam_distrib* distrib)
{
  ASSERT(distrib);
  return distrib->count;
}

double*
cmc_dynam_distrib_get_energy(const struct cmc_dynam_distrib* distrib)
{
  ASSERT(distrib);
  return distrib->energy_value;
}

double* 
cmc_dynam_distrib_get_density(const struct cmc_dynam_distrib* distrib)
{
  ASSERT(distrib);
  return distrib->density_value;
}
