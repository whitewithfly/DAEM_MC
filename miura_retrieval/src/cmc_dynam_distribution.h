/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef CMC_DISTRIB_H
#define CMC_DISTRIB_H

#include <rsys/rsys.h>

struct cmc_dynam;
struct cmc_dynam_distrib;


/*******************************************************************************
 * tests functions
 ******************************************************************************/
extern LOCAL_SYM res_T
cmc_dynam_distrib_create
  (struct cmc_dynam* cmc_dynam,
   const char* distrib_props_filename,
   struct cmc_dynam_distrib** distrib);

extern LOCAL_SYM void
cmc_dynam_distrib_ref_get
  (struct cmc_dynam_distrib* distrib);

extern LOCAL_SYM void
cmc_dynam_distrib_ref_put
  (struct cmc_dynam_distrib* distrib);

extern LOCAL_SYM int  cmc_dynam_distrib_get_count(const struct cmc_dynam_distrib* distrib);
extern LOCAL_SYM double*  cmc_dynam_distrib_get_energy(const struct cmc_dynam_distrib* distrib);
extern LOCAL_SYM double*  cmc_dynam_distrib_get_density(const struct cmc_dynam_distrib* distrib);

extern LOCAL_SYM res_T
compute_cumulative_distribution
  (struct cmc_dynam* cmc_dynam);

#endif /* CMC_DISTRIB_H */
