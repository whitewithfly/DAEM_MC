/* Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include <star/ssp.h>       
#include <star/smc.h>       

#include <gsl/gsl_integration.h>

#include "cmc_dynam.h"
#include "cmc_dynam_args.h"
#include "cmc_dynam_log.h"
#include "cmc_dynam_distribution.h"
#include "cmc_dynam_compute_rate.h"
#include "cmc_dynam_deterministic.h"

#include <rsys/clock_time.h>
#include <rsys/cstr.h>
#include <rsys/mutex.h>
#include <rsys/str.h>

res_T
cmc_dynam_init
  (const struct cmc_dynam_args* args,
   struct cmc_dynam* cmc_dynam)
{
  res_T res = RES_OK;
  ASSERT(args && cmc_dynam);

  memset(cmc_dynam, 0, sizeof(*cmc_dynam)); /* Reset memory */

  setup_logger(cmc_dynam);

	/* Initialize global parameters */
  cmc_dynam->nrealisations = args->nrealisations;
  cmc_dynam->deter = (int)args->deter;
  cmc_dynam->init_temperature = args->init_temperature;
  cmc_dynam->final_time = args->final_time;
	cmc_dynam->beta = 10.0/60.0;


	/* Initialize Miura distribution */
	res = cmc_dynam_distrib_create(cmc_dynam, args->path_miura_file, &cmc_dynam->distrib);
  if(res != RES_OK) goto error;

  /* Create the Star-MonteCarlo device */
  res = smc_device_create
    (&cmc_dynam->logger, NULL, SMC_NTHREADS_DEFAULT, NULL, &cmc_dynam->smc);
  if(res != RES_OK) {
    cmc_dynam_log_err(cmc_dynam,
      "Could not create the Star-MonteCarlo device -- %s.\n",
      res_to_cstr(res));
    goto error;
  }
  SMC(device_get_threads_count(cmc_dynam->smc, &cmc_dynam->nthreads));

	/* Shooting values */
  cmc_dynam_log(cmc_dynam, "Time of interest: %g second(s)\n", cmc_dynam->final_time);
  cmc_dynam_log(cmc_dynam, "Initial temperature : %g Kelvin\n", cmc_dynam->init_temperature);
  cmc_dynam_log(cmc_dynam, "For energy : %g\t",	cmc_dynam_distrib_get_energy(cmc_dynam->distrib)[0]);
  cmc_dynam_log(cmc_dynam, "the energy density is  : %g\n", 
		cmc_dynam_distrib_get_density(cmc_dynam->distrib)[0]);
  cmc_dynam_log(cmc_dynam, "#realisations: %ld\n", cmc_dynam->nrealisations);

exit:
  return res;
error:
  cmc_dynam_release(cmc_dynam);
  goto exit;
}

void
cmc_dynam_release(struct cmc_dynam* cmc_dynam)
{
  ASSERT(cmc_dynam);
  logger_release(&cmc_dynam->logger);
}

res_T
cmc_dynam_run(struct cmc_dynam* cmc_dynam)
{
  struct smc_integrator integrator;
  struct smc_estimator* estimator = NULL;
  struct smc_estimator_status estimator_status;
  struct time t0, t1;
  char dump[64];
  res_T res = RES_OK;

	/* Compute the cumulative distribution function from miura */
	res = compute_cumulative_distribution(cmc_dynam);	

	/* Setup Star-MC */
	cmc_dynam->index_realisation = 0;
	integrator.integrand = &daem_realization; /* Realization function */
	integrator.type = &smc_double; /* Type of the Monte Carlo weight */
	integrator.max_realisations = cmc_dynam->nrealisations; /* Realization count */
	integrator.max_failures = cmc_dynam->nrealisations / 1000;
  time_current(&t0);
	
	/* Monte-Carlo estimation */
	SMC(solve(cmc_dynam->smc, &integrator, cmc_dynam, &estimator));
	
	/* Print the simulation results */
	SMC(estimator_get_status(estimator, &estimator_status));
  cmc_dynam_log(cmc_dynam, "Monte-Carlo estimation ");
	printf("%g %g %g",
		cmc_dynam->final_time, SMC_DOUBLE(estimator_status.E),SMC_DOUBLE(estimator_status.SE)); 

	/* Gauss-Hermite estimation */
	if(cmc_dynam->deter == 1){
		double result;
		
  	cmc_dynam_log(cmc_dynam, "With determinist calculus\n");
		res = deter(cmc_dynam, &result);
		if(res != RES_OK) {		
			res = RES_BAD_ARG;
			goto error;
		}
		printf (" %g ", result);
	}
	
	time_sub(&t0, time_current(&t1), &t0);
	time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
	printf(" Computation time: %s\n", dump);

exit:
  return res;
error:
  cmc_dynam_release(cmc_dynam);
  goto exit;
}
