# Copyright (C) 2021 UPS (yaniss.nyffenegger-pere@laplace.univ-tlse.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.12)
project(cmc_dynam)
enable_language(C)
set(CMC_DYNAM_SOURCE_DIR ${PROJECT_SOURCE_DIR}/../src)

################################################################################
# Check dependencies
################################################################################
find_package(RCMake 0.3 REQUIRED)
find_package(RSys 0.6 REQUIRED)
find_package(StarSP 0.7 REQUIRED)
find_package(StarMC 0.4 REQUIRED)

include_directories(
  ${RSys_INCLUDE_DIR}
  ${StarSP_INCLUDE_DIR}
  ${StarMC_INCLUDE_DIR})

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${RCMAKE_SOURCE_DIR})
include(rcmake)
include(rcmake_runtime)

################################################################################
# Configure and define the targets
################################################################################
set(VERSION_MAJOR 0)
set(VERSION_MINOR 0)
set(VERSION_PATCH 1)
set(VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH})

# CMC_DYNAM
set(CMC_DYNAM_FILES_SRC 
  cmc_dynam.c
  cmc_dynam_args.c
  cmc_dynam_deterministic.c
  cmc_dynam_main.c
  cmc_dynam_compute_rate.c
  cmc_dynam_log.c)

set(CMC_DYNAM_FILES_INC   
  cmc_dynam.h
  cmc_dynam_args.h
  cmc_dynam_deterministic.h
	cmc_dynam_compute_rate.h
  cmc_dynam_log.h)

# Prepend each file in the `CMC_DYNAM_FILES_<SRC|INC>' list by the absolute
# path of the directory in which they lie
rcmake_prepend_path(CMC_DYNAM_FILES_SRC ${CMC_DYNAM_SOURCE_DIR})
rcmake_prepend_path(CMC_DYNAM_FILES_INC ${CMC_DYNAM_SOURCE_DIR})

add_executable(cmc_dynam ${CMC_DYNAM_FILES_SRC} ${CMC_DYNAM_FILES_INC})
target_link_libraries(cmc_dynam PRIVATE
	RSys 
	StarSP 
	StarMC 
	gsl
	m)

