rm *.out

for time in {700..2100..5}
do 
	# Temperature 
	for temperature in {400..500..50}
	do 
		../build/cmc_dynam -n 1 -A 1e14 -t $time -T $temperature -w 3000 -d >> deter_$temperature.out 
	done 
done 

for time in {700..2100..20}
do
	# Temperature 
	for temperature in {400..500..50}
	do 
		../build/cmc_dynam -n 40000 -A 1e14 -t $time -T $temperature -w 3000 -d >> mc_$temperature.out 
	done
done
